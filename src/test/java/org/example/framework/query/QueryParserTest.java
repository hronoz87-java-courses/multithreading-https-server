package org.example.framework.query;


import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import static org.example.framework.auth.principal.AnonymousPrincipal.ANONYMOUS;
import static org.junit.jupiter.api.Assertions.*;

class QueryParserTest {
    final Principal principal = () -> ANONYMOUS;
    final Matcher matcher = null;
    final QueryParser queryParser = new QueryParser();

    @Test
    void shouldQueryParserParse() {
        final Map<String, List<String>> expected = new LinkedHashMap<>();
        final List<String> values = new ArrayList<>();
        values.add("java");
        expected.put("text", values);
        final List<String> params = new ArrayList<>();
        params.add("ebook");
        params.add("course");
        expected.put("type", params);


        final Request request = new Request("method=GET", "path=/items.getAll", matcher,
                "text=java&type=ebook&type=course", "httpVersion=HTTP/1.1",
                null, null, null, null, null, principal);

        queryParser.parse(request);
        final Map<String, List<String>> actual = request.getQueryParams();

        assertEquals(expected, actual);

    }

    @Test
    void shouldQueryParserParseQueryEmpty() {
        final Request request = new Request("method=GET", "path=/items.getAll", matcher, "",
                "httpVersion=HTTP/1.1", null,
                null, null, null, null, principal);

        assertThrows(QueryParseException.class, () -> queryParser.parse(request));

    }

    @Test
    void shouldQueryParserParseQueryNull() {
        final Request query = new Request("method=GET", "path=/items.getAll", matcher, null,
                "httpVersion=HTTP/1.1", null,
                null, null, null, null, principal);
        final Map<String, List<String>> actual = query.getQueryParams();

        queryParser.parse(query);
        final Map<String, List<String>> expected = query.getQueryParams();

        assertEquals(expected, actual);
    }
}