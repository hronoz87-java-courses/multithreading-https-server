package org.example.framework.multipart;

import org.example.framework.exception.InvalidHeaderLineStructureException;
import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


class MultiPartParserTest {

    final MultiPartParser multiPartParser = new MultiPartParser();

    @Test
    void shouldMultiParserParse() throws IOException {
        final Map<String, List<Part>> map = new LinkedHashMap<>();
        final List<Part> filePart = new LinkedList<>();
        final Map<String, String> headersFilePart = new LinkedHashMap<>();
        headersFilePart.put("Content-Disposition", "form-data");
        headersFilePart.put("Content-Type", "text/plain");
        headersFilePart.put("Content-Transfer-Encoding", "8bit");
        filePart.add(new FormFieldPart("name", "Petya", headersFilePart));
        map.put("name", filePart);

        final List<Part> formPart = new ArrayList<>();
        final Map<String, String> headersForm = new LinkedHashMap<>();
        headersForm.put("Content-Disposition", "form-data");
        headersForm.put("Content-Type", "image/png");
        headersForm.put("Content-Transfer-Encoding", "binary");
        final byte[] bytes = "-> байты <--".getBytes(StandardCharsets.UTF_8);
        formPart.add(new FilePart("image", "image.png", bytes, headersForm));
        map.put("image", formPart);

        final Map<String, List<Part>> expected = map;

        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/body.txt"));
        request.setBody(array);
        multiPartParser.parse(request);
        final Map<String, List<Part>> actual = request.getAllMultipartParts();
        assertEquals(expected, actual);
    }

    @Test
    void shouldMultiParserParseDoubleValues() throws IOException {
        final Map<String, List<Part>> map = new LinkedHashMap<>();
        final List<Part> filePart = new LinkedList<>();
        final Map<String, String> headersFilePart = new LinkedHashMap<>();
        headersFilePart.put("Content-Disposition", "form-data");
        headersFilePart.put("Content-Type", "text/plain");
        headersFilePart.put("Content-Transfer-Encoding", "8bit");
        filePart.add(new FormFieldPart("name", "Vasya", headersFilePart));
        map.put("name", filePart);

        headersFilePart.put("Content-Disposition", "form-data");
        headersFilePart.put("Content-Type", "text/plain");
        headersFilePart.put("Content-Transfer-Encoding", "8bit");
        filePart.add(new FormFieldPart("name", "Petya", headersFilePart));
        map.put("name", filePart);

        final List<Part> formPart = new ArrayList<>();
        final Map<String, String> headersForm = new LinkedHashMap<>();
        headersForm.put("Content-Disposition", "form-data");
        headersForm.put("Content-Type", "image/png");
        headersForm.put("Content-Transfer-Encoding", "binary");
        final byte[] bytes = "-> байты <--".getBytes(StandardCharsets.UTF_8);
        formPart.add(new FilePart("image", "image.png", bytes, headersForm));
        map.put("image", formPart);

        final Map<String, List<Part>> expected = map;

        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodyDouble.txt"));
        request.setBody(array);
        multiPartParser.parse(request);
        final Map<String, List<Part>> actual = request.getAllMultipartParts();
        assertEquals(expected, actual);
    }

    @Test
    void shouldMultiParserParseDoubleValuesTwo() throws IOException {
        final Map<String, List<Part>> map = new LinkedHashMap<>();
        final List<Part> filePart = new LinkedList<>();
        final Map<String, String> headersFilePart = new LinkedHashMap<>();
        headersFilePart.put("Content-Disposition", "form-data");
        headersFilePart.put("Content-Type", "text/plain");
        headersFilePart.put("Content-Transfer-Encoding", "8bit");
        filePart.add(new FormFieldPart("name", "Petya", headersFilePart));
        map.put("name", filePart);

        final List<Part> formPart = new ArrayList<>();
        final Map<String, String> headersForm = new LinkedHashMap<>();
        headersForm.put("Content-Disposition", "form-data");
        headersForm.put("Content-Type", "image/png");
        headersForm.put("Content-Transfer-Encoding", "binary");
        final byte[] bytes = "-> байты <--".getBytes(StandardCharsets.UTF_8);
        formPart.add(new FilePart("image", "image.png", bytes, headersForm));
        map.put("image", formPart);

        headersForm.put("Content-Disposition", "form-data");
        headersForm.put("Content-Type", "image/png");
        headersForm.put("Content-Transfer-Encoding", "binary");
        final byte[] bytesOne = "-> байты <--".getBytes(StandardCharsets.UTF_8);
        formPart.add(new FilePart("image", "image.png", bytesOne, headersForm));
        map.put("image", formPart);

        final Map<String, List<Part>> expected = map;

        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodyDoubleTwo.txt"));
        request.setBody(array);
        multiPartParser.parse(request);
        final Map<String, List<Part>> actual = request.getAllMultipartParts();
        assertEquals(expected, actual);
    }

    @Test
    void shouldQMultiParserContentTypeNotMultipart() throws IOException {
        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "text/plain; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/body.txt"));
        request.setBody(array);
        final Map<String, List<Part>> actual = request.getAllMultipartParts();

        multiPartParser.parse(request);
        final Map<String, List<Part>> expected = request.getAllMultipartParts();

        assertEquals(expected, actual);
    }

    @Test
    void shouldQMultiParserContentDispositionMoreParserCanHandle() throws IOException {
        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodyDisposition.txt"));
        request.setBody(array);
        assertThrows(MultipartParseException.class, () -> multiPartParser.parse(request));
    }

    @Test
    void shouldQMultiParserContentTypeNotMultipartException() throws IOException {
        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodys.txt"));
        request.setBody(array);

        assertThrows(InvalidHeaderLineStructureException.class, () -> multiPartParser.parse(request));
    }

    @Test
    void shouldQMultiParserContentTypeNotMultipartExceptionBoundary() throws IOException {
        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodys.txt"));
        request.setBody(array);

        assertThrows(MultipartParseException.class, () -> multiPartParser.parse(request));
    }

    @Test
    void shouldQMultiParserContentTypeNotMultipartExceptionNotLastIndex() throws IOException {
        final Request request = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", "multipart/form-data; boundary=--WebAppBoundary");
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        request.setMethod("POST");
        request.setPath("/debug");
        request.setHttpVersion("HTTP/1.1");
        request.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodyLF"));
        request.setBody(array);

        assertThrows(MultipartParseException.class, () -> multiPartParser.parse(request));
    }

    @Test
    void shouldQMultiParserParseContentTypeNull() throws IOException {
        final Request multi = new Request();
        final Map<String, String> headers = new LinkedHashMap<>();
        headers.put("Host", "localhost:9999");
        headers.put("Content-Type", null);
        headers.put("Content-Length", "449");
        headers.put("Connection", "Keep-Alive");
        headers.put("User-Agent", "Apache-HttpClient/4.5.13 (Java/11.0.13)");
        headers.put("Accept-Encoding", "gzip,deflate");
        multi.setMethod("POST");
        multi.setPath("/debug");
        multi.setHttpVersion("HTTP/1.1");
        multi.setHeaders(headers);
        final byte[] array = Files.readAllBytes(Paths.get("src/test/resources/bodys.txt"));
        multi.setBody(array);
        final Map<String, List<Part>> actual = multi.getAllMultipartParts();

        multiPartParser.parse(multi);
        final Map<String, List<Part>> expected = multi.getAllMultipartParts();

        assertEquals(expected, actual);
    }
}
