package org.example.framework.form;

import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;
import java.util.regex.Matcher;

import static org.example.framework.auth.principal.AnonymousPrincipal.ANONYMOUS;
import static org.junit.jupiter.api.Assertions.*;

class FormParserTest {
    final Principal principal = () -> ANONYMOUS;
    final Matcher matcher = null;
    final FormParser formParser = new FormParser();

    @Test
    void shouldFormParserParse() throws UnsupportedEncodingException {
        final Map<String, List<String>> expected = new LinkedHashMap<>();
        final List<String> values1 = new ArrayList<>();
        values1.add("+79xx-xxx-xxxx");
        expected.put("phone", values1);

        final String method = "POST";
        final String path = "/items.getAll?";
        final String query = "type=ebook";
        final String httpVersion = "HTTP/1.1";
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final Map<String, String> headers = new LinkedHashMap<>();
        final byte[] body = {112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120};

        headers.put("Content-Type", "application/x-www-form-urlencoded");

        final Request request = new Request(method, path, matcher, query, httpVersion, null, formParams, null, headers, body, principal);

        formParser.parse(request);
        final Map<String, List<String>> actual = request.getFormParams();

        assertEquals(expected, actual);
    }

    @Test
    void shouldFormParserParseEqualsIgnore() throws UnsupportedEncodingException {
        final Map<String, List<String>> expected = new LinkedHashMap<>();
        final List<String> values = new ArrayList<>();
        final List<String> values1 = new ArrayList<>();
        values1.add("+79xx-xxx-xxxx");
        values1.add("+79xx-xxx-xxxx");
        expected.put("phone", values1);

        final String method = "POST";
        final String path = "/items.getAll?";
        final String query = "type=ebook&text=java&type=course";
        final String httpVersion = "HTTP/1.1";
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final Map<String, String> headers = new LinkedHashMap<>();
        final byte[] body = {112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120,
                38, 112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120};


        headers.put("Content-Type", "Application/x-www-form-urlencoded");

        final Request request = new Request(method, path, matcher, query, httpVersion, null, formParams, null, headers, body, principal);

        formParser.parse(request);
        final Map<String, List<String>> actual = request.getFormParams();

        assertEquals(expected, actual);
    }

    @Test
    void shouldFormParserParseContentTypeNotXForm() throws UnsupportedEncodingException {
        final Map<String, List<String>> expected = new LinkedHashMap<>();
        final List<String> values = new ArrayList<>();
        values.add("ebook");
        values.add("course");
        expected.put("type", values);
        final List<String> values2 = new ArrayList<>();
        values2.add("java");
        expected.put("text", values2);
        final List<String> values1 = new ArrayList<>();
        values1.add("+79xx-xxx-xxxx");
        values1.add("+79xx-xxx-xxxx");
        expected.put("phone", values1);

        final String method = "POST";
        final String path = "/items.getAll?";
        final String query = "type=ebook&text=java&type=course";
        final String httpVersion = "HTTP/1.1";
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final Map<String, String> headers = new LinkedHashMap<>();
        final byte[] body = {112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120,
                38, 112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120};

        headers.put("Content-Type", "Application/x-www-form-urlencoder");

        final Request request = new Request(method, path, matcher, query, httpVersion, null, formParams, null, headers, body, principal);

        formParser.parse(request);
        final Map<String, List<String>> actual = request.getFormParams();

        assertNotEquals(expected, actual);
    }

    @Test
    void shouldFormParserParseNull() throws UnsupportedEncodingException {
        final String method = "POST";
        final String path = "/items.getAll?";
        final String query = "type=ebook&text=java&type=course";
        final String httpVersion = "HTTP/1.1";
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final Map<String, String> headers = new LinkedHashMap<>();
        final byte[] body = {112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120,
                38, 112, 104, 111, 110, 101, 61, 37, 50, 66, 55, 57, 120, 120, 45, 120, 120, 120, 45, 120, 120, 120, 120};
        headers.put("Content-Type", null);

        final Request expected = new Request(method, path, matcher, query, httpVersion, null, formParams, null, headers, body, principal);

        formParser.parse(expected);
        final Map<String, List<String>> form = expected.getFormParams();
        final Request actual = new Request(method, path, matcher, query, httpVersion, null, form, null, headers, body, principal);

        assertEquals(expected, actual);
    }

    @Test
    void shouldFormParserParseFormException() {
        final String method = "POST";
        final String path = "/items.getAll?";
        final String query = "type=ebook";
        final String httpVersion = "HTTP/1.1";
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final Map<String, String> headers = new LinkedHashMap<>();
        final byte[] body = {45, 120, 120, 120, 120};

        headers.put("Content-Type", "Application/x-www-form-urlencoded");

        final Request request = new Request(method, path, matcher, query, httpVersion, null, formParams, null, headers, body, principal);
        assertThrows(FormParseException.class, () -> formParser.parse(request));
    }
}
