package org.example.framework.auth;

public interface Authenticator {
    boolean authenticate(final AuthenticationToken request);
}
