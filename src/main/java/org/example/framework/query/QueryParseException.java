package org.example.framework.query;

public class QueryParseException extends RuntimeException {

    public QueryParseException(String message) {
        super(message);
    }
}
