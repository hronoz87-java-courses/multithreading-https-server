package org.example.framework.query;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;

import java.util.*;


@Slf4j
public class QueryParser implements Parser {

    public QueryParser() {
    }

    @Override
    public void parse(final Request request) {
        if (request.getQuery() == null) {
            return;
        }
        final Map<String, List<String>> queryParams = new LinkedHashMap<>();
        final String query = request.getQuery();
        final String[] queryKeyAndValueParams = query.split("\\&");
        for (String queryKeyAndValueParam : queryKeyAndValueParams) {
            final String[] queryKeyAndParam = queryKeyAndValueParam.split("\\=");
            if (queryKeyAndParam.length < 2) {
                throw new QueryParseException("Error parse");
            }
            final String queryKey = queryKeyAndParam[0];
            final String queryValue = queryKeyAndParam[1];
            if (!queryParams.containsKey(queryKey)) {
                queryParams.put(queryKey, new LinkedList<>());
            }
            queryParams.get(queryKey).add(queryValue);
        }
        request.setQueryParams(queryParams);
    }
}
