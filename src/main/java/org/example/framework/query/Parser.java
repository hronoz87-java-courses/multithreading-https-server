package org.example.framework.query;


import org.example.framework.http.Request;

import java.io.UnsupportedEncodingException;

public interface Parser {
    void parse(Request request) throws UnsupportedEncodingException;
}
