package org.example.framework.multipart;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.InvalidHeaderLineStructureException;
import org.example.framework.http.HttpHeaders;
import org.example.framework.http.Request;
import org.example.framework.query.Parser;
import org.example.util.Bytes;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class MultiPartParser implements Parser {
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public static final Pattern BOUNDARY_PATTERN = Pattern.compile("boundary=([-\"A-Za-z0-9]+)");
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};


    public void parse(final Request request) {
        Map<String, List<Part>> multipartParts = new LinkedHashMap<>();
        final String contentType = request.getHeaders().get(HttpHeaders.CONTENT_TYPE.value());
        if (contentType == null) {
            return;
        }
        if (!contentType.startsWith(MULTIPART_FORM_DATA)) {
            return;
        }

        final String boundary = extractBoundary(contentType);
        final byte[] partStartBoundary = ("--" + boundary).getBytes(StandardCharsets.UTF_8);
        final byte[] lastPartEndBoundary = ("--" + boundary + "--").getBytes(StandardCharsets.UTF_8);

        final byte[] body = request.getBody();
        final int lastPartEndIndex = Bytes.indexOf(body, lastPartEndBoundary);
        if (lastPartEndIndex == -1) {
            throw new MultipartParseException("can't find last part end boundary");
        }

        int lastProcessedIndex = 0;
        while (lastProcessedIndex < lastPartEndIndex) {
            final int partHeadersStartIndex = Bytes.indexOf(body, partStartBoundary, lastProcessedIndex) + partStartBoundary.length + CRLF.length;
            final int partHeadersEndIndex = Bytes.indexOf(body, CRLFCRLF, partHeadersStartIndex);
            final int partBodyStartIndex = partHeadersEndIndex + CRLFCRLF.length;
            final int partBodyEndIndex = Bytes.indexOf(body, partStartBoundary, partBodyStartIndex) - CRLF.length;
            lastProcessedIndex = partBodyEndIndex + CRLF.length;

            final Map<String, String> partHeaders = extractPartHeaders(body, partHeadersStartIndex, partHeadersEndIndex);

            final int partBodyLength = partBodyEndIndex - partBodyStartIndex;
            final byte[] partBody = new byte[partBodyLength];
            System.arraycopy(body, partBodyStartIndex, partBody, 0, partBodyLength);

            final Part part = createPart(partHeaders, partBody);
            final List<Part> parts = new LinkedList<>();
            parts.add(part);
            if (!(multipartParts.containsKey(parts.get(0).getName()))) {
                multipartParts.put(parts.get(0).getName(), new LinkedList<>());
            }
            multipartParts.get(parts.get(0).getName()).add(part);
        }
        request.setMultipartParts(multipartParts);
    }

    private Part createPart(final Map<String, String> partHeaders, final byte[] partBody) {
        final String nameAndFileName = partHeaders.get(HttpHeaders.CONTENT_DISPOSITION.value());
        final String[] splitHeaderFromName = nameAndFileName.split("\\; ");
        if (splitHeaderFromName.length == 2) {
            partHeaders.put(HttpHeaders.CONTENT_DISPOSITION.value(), splitHeaderFromName[0]);
            final String nameKeyAndValue = splitHeaderFromName[1];
            final String[] splitKeyAndValue = nameKeyAndValue.split("\"");
            return new FormFieldPart(splitKeyAndValue[1], new String(partBody), partHeaders);
        }
        if (splitHeaderFromName.length == 3) {
            partHeaders.put(HttpHeaders.CONTENT_DISPOSITION.value(), splitHeaderFromName[0]);
            final String name = splitHeaderFromName[1];
            final String[] splitKeyAndValue = name.split("\"");
            final String fileName = splitHeaderFromName[2];
            final String[] fileNameValue = fileName.split("\"");
            return new FilePart(splitKeyAndValue[1], fileNameValue[1], partBody, partHeaders);
        }
        throw new MultipartParseException("the length of the data is more than the parser can handle");
    }

    private static Map<String, String> extractPartHeaders(final byte[] body, final int partHeadersStartIndex, final int partHeadersEndIndex) {
        final Map<String, String> partHeaders = new LinkedHashMap<>();
        int lastProcessedIndex = partHeadersStartIndex;
        while (lastProcessedIndex < partHeadersEndIndex) {
            final int currentHeaderEndIndex = Bytes.indexOf(body, CRLF, lastProcessedIndex);
            final String currentHeaderLine = new String(body, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
            lastProcessedIndex = currentHeaderEndIndex + CRLF.length;

            final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
            if (headerParts.length != 2) {
                throw new InvalidHeaderLineStructureException(currentHeaderLine);
            }
            partHeaders.put(headerParts[0], headerParts[1]);
        }
        return partHeaders;
    }

    public static String extractBoundary(final String contentType) {
        final Matcher matcher = BOUNDARY_PATTERN.matcher(contentType);
        if (!matcher.find()) {
            throw new MultipartParseException("no boundary in content-type");
        }

        return matcher.group(1);
    }
}


