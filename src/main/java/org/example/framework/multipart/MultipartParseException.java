package org.example.framework.multipart;

public class MultipartParseException extends RuntimeException {
    public MultipartParseException(String message) {
        super(message);
    }
}
