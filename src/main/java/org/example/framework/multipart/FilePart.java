package org.example.framework.multipart;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public class FilePart implements Part {
    final String name;
    final String fileName;
    final byte[] content;
    final Map<String, String> headers;

    public FilePart(String name, String fileName, byte[] content, Map<String, String> headers) {
        this.name = name;
        this.fileName = fileName;
        this.content = content;
        this.headers = headers;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getContent() {
        return content;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", fileName='" + fileName + '\'' +
                ", content=" + Arrays.toString(content) +
                ", headers=" + headers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FilePart filePart = (FilePart) o;
        return Objects.equals(name, filePart.name) && Objects.equals(fileName, filePart.fileName)
                && Arrays.equals(content, filePart.content) && Objects.equals(headers, filePart.headers);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, fileName, headers);
        result = 31 * result + Arrays.hashCode(content);
        return result;
    }
}
