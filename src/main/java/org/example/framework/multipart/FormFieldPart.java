package org.example.framework.multipart;

import java.util.Map;
import java.util.Objects;

public class FormFieldPart implements Part {
    final String name;
    final String value;
    final Map<String, String> headers;

    public FormFieldPart(String name, String value, Map<String, String> headers) {
        this.name = name;
        this.value = value;
        this.headers = headers;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", headers=" + headers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FormFieldPart that = (FormFieldPart) o;
        return Objects.equals(name, that.name) && Objects.equals(value, that.value) && Objects.equals(headers, that.headers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, headers);
    }
}
