package org.example.framework.exception;

public class RequestBodyTooLargeExeption extends RuntimeException {
    public RequestBodyTooLargeExeption() {
        super();
    }

    public RequestBodyTooLargeExeption(String message) {
        super(message);
    }

    public RequestBodyTooLargeExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestBodyTooLargeExeption(Throwable cause) {
        super(cause);
    }

    protected RequestBodyTooLargeExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
