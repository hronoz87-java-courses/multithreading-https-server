package org.example.framework.exception;

public class InvalidHeaderLineStructureException extends RuntimeException {
    public InvalidHeaderLineStructureException() {
        super();
    }

    public InvalidHeaderLineStructureException(String message) {
        super(message);
    }

    public InvalidHeaderLineStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidHeaderLineStructureException(Throwable cause) {
        super(cause);
    }

    protected InvalidHeaderLineStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
