package org.example.framework.exception;

public class UnsupportedAuthenticationToken extends RuntimeException{
    public UnsupportedAuthenticationToken() {
    }

    public UnsupportedAuthenticationToken(String message) {
        super(message);
    }

    public UnsupportedAuthenticationToken(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedAuthenticationToken(Throwable cause) {
        super(cause);
    }

    public UnsupportedAuthenticationToken(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
