package org.example.framework.exception;

public class MarkNotSupportedException extends RuntimeException {
    public MarkNotSupportedException() {
        super();
    }

    public MarkNotSupportedException(String message) {
        super(message);
    }

    public MarkNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MarkNotSupportedException(Throwable cause) {
        super(cause);
    }

    protected MarkNotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
