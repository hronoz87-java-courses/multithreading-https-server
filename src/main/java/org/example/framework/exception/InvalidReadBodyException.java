package org.example.framework.exception;

public class InvalidReadBodyException extends RuntimeException {

    public InvalidReadBodyException() {
        super();
    }

    public InvalidReadBodyException(String message) {
        super(message);
    }

    public InvalidReadBodyException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidReadBodyException(Throwable cause) {
        super(cause);
    }

    protected InvalidReadBodyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
