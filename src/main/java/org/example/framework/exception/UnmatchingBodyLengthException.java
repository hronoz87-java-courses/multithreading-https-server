package org.example.framework.exception;

public class UnmatchingBodyLengthException extends RuntimeException {

    public UnmatchingBodyLengthException() {
        super();
    }

    public UnmatchingBodyLengthException(String message) {
        super(message);
    }

    public UnmatchingBodyLengthException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnmatchingBodyLengthException(Throwable cause) {
        super(cause);
    }

    protected UnmatchingBodyLengthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
