package org.example.framework.exception;

public class InvalidRequestStructureException extends RuntimeException {
    public InvalidRequestStructureException() {
        super();
    }

    public InvalidRequestStructureException(String message) {
    }

    public InvalidRequestStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRequestStructureException(Throwable cause) {
        super(cause);
    }

    protected InvalidRequestStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
