package org.example.framework.form;

public class FormParseException extends RuntimeException {

    public FormParseException(String message) {
        super(message);
    }
}
