package org.example.framework.form;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import org.example.framework.query.Parser;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
public class FormParser implements Parser {

    public FormParser() {
    }

    @Override
    public void parse(final Request request) throws UnsupportedEncodingException {
        final String contentTypeKey = request.getHeaders().get("Content-Type");
        if (contentTypeKey == null) {
            return;
        }
        if (!(contentTypeKey.equalsIgnoreCase("application/x-www-form-urlencoded"))) {
            return;
        }
        final Map<String, List<String>> formParams = new LinkedHashMap<>();
        final byte[] body = request.getBody();
        final String bodyString = new String(body, StandardCharsets.UTF_8);
        final String[] bodyQuery = bodyString.split("\\&");
        for (String s : bodyQuery) {
            final String[] queryThisBody = s.split("\\=");
            if (queryThisBody.length != 2) {
                throw new FormParseException("incorrect body for the request ");
            }
            final String queryKeyBody = queryThisBody[0];
            final String queryValueBody = URLDecoder.decode(queryThisBody[1], String.valueOf(StandardCharsets.UTF_8));
            if (!formParams.containsKey(queryKeyBody)) {
                formParams.put(queryKeyBody, new LinkedList<>());
            }
            formParams.get(queryKeyBody).add(queryValueBody);
        }
        request.setFormParams(formParams);

    }
}
