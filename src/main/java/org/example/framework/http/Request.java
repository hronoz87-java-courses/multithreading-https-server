package org.example.framework.http;

import lombok.*;
import org.example.framework.multipart.FilePart;
import org.example.framework.multipart.FormFieldPart;
import org.example.framework.multipart.Part;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
    private String method;
    private String path;
    private Matcher pathMatcher;
    private String query;
    private String httpVersion;
    private Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private Map<String, List<String>> formParams = new LinkedHashMap<>();
    private Map<String, List<Part>> multipartParts = new LinkedHashMap<>();
    private Map<String, String> headers = new LinkedHashMap<>();
    private byte[] body;
    private Principal principal;

    public String getPathGroup(final String name) {
        return pathMatcher.group(name);
    }

    public String getPathGroup(final int index) {
        return pathMatcher.group(index);
    }


    public Optional<String> getQueryParam(final String param) {
        return Optional.ofNullable(queryParams.get(param).get(0));
    }

    public Optional<List<String>> getQueryParams(final String param) {
        return Optional.ofNullable(queryParams.get(param));
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }

    public Optional<String> getFormParam(final String param) {
        return Optional.ofNullable(formParams.get(param).get(0));
    }

    public Optional<List<String>> getFormParams(final String param) {
        return Optional.ofNullable(formParams.get(param));
    }

    public Map<String, List<String>> getAllFormParams() {
        return formParams;
    }

    public Optional<FormFieldPart> getMultipartFormFieldPart(String param) {
        return Optional.ofNullable(multipartParts.get(param))
                .map(o -> ((FormFieldPart) o.get(0)));
    }

    public Optional<FilePart> getMultipartPart(String param) {
        return Optional.ofNullable(multipartParts.get(param))
                .map(o -> ((FilePart) o.get(0)));

    }

    public Optional<List<Part>> getMultipartParts(final String param) {
        return Optional.ofNullable(multipartParts.get(param));
    }

    public Map<String, List<Part>> getAllMultipartParts() {
        return multipartParts;
    }
}
