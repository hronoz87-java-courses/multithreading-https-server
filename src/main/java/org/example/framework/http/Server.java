package org.example.framework.http;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.SecurityContext;
import org.example.framework.exception.*;
import org.example.framework.handler.Handler;
import org.example.framework.query.Parser;
import org.example.middleware.Middleware;
import org.example.util.Bytes;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};
    private static final int MAX_CONTENT_LENGTH = 1024 * 1024 * 10;
    private final List<Middleware> middlewares;
    private final List<Parser> parsers;
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    private final Handler notFoundHandler = Handler::notFoundHandler;
    private final Handler internalServerErrorHandler = Handler::internalServerError;
    private final Handler methodNotAllowedHandler = Handler::methodNotAllowedHandler;

    private final AtomicInteger workerCounter = new AtomicInteger();
    private final ExecutorService workers = new ThreadPoolExecutor(64, 200, 60, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(),
            r -> {
                final Thread worker = new Thread(r);
                worker.setName("worker-" + workerCounter.incrementAndGet());
                return worker;
            });

    public Server(List<Middleware> middlewares, List<Parser> parsers, Map<Pattern, Map<HttpMethods, Handler>> routes,
                  Handler notFoundHandler, Handler internalServerErrorHandler, Handler methodNotAllowedHandler) {
        this.middlewares = middlewares;
        this.parsers = parsers;
        this.routes = routes;
    }

    private static Handler notFoundHandler() {
        return Handler::notFoundHandler;
    }

    private static Handler internalServerErrorHandler() {
        return Handler::internalServerError;
    }

    private static Handler methodNotAllowedHandler() {
        return Handler::methodNotAllowedHandler;
    }

    public static ServerBuilder builder() {
        return new ServerBuilder();
    }

    private ServerSocket serverSocket;

    public void serveHttps(final int port) {
        final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
        try {
            serverSocket = socketFactory.createServerSocket(port);
            final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
            sslServerSocket.setWantClientAuth(true);
            log.info("Server listen on {} : {}", serverSocket.getInetAddress().getHostName(), port);
            new Thread(() -> {
                while (true) {
                    try {
                        final SSLSocket socket = (SSLSocket) serverSocket.accept();
                        workers.submit(() -> serve(socket));
                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }
                    } catch (Exception e) {
                        log.error("some error", e);
                        return;
                    }
                }
            }).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void serveHttp(final int port) {
        try {
            final ServerSocket serverSocket = new ServerSocket(port);
            log.info("Server listen on {} : {}", serverSocket.getInetAddress().getHostName(), port);
            new Thread(() -> {
                while (true) {
                    try {
                        final SSLSocket socket = (SSLSocket) serverSocket.accept();
                        workers.submit(() -> serve(socket));
                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }
                    } catch (Exception e) {
                        log.error("some error", e);
                        return;
                    }
                }
            }).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void serve(final Socket acceptedSocket) {
        try (
                final Socket socket = acceptedSocket;
                final InputStream in = new BufferedInputStream(socket.getInputStream());
                final OutputStream out = socket.getOutputStream()
        ) {
            log.debug("client connected: {}:{} ", socket.getInetAddress().getHostName(), socket.getPort());
            Request request = new Request();
            try {
                final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];

                if (!in.markSupported()) {
                    throw new MarkNotSupportedException();
                }
                in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

                final int firstRead = in.read(buffer);

                final int requestLineAndIndex = Bytes.indexOf(buffer, CRLF);
                if (requestLineAndIndex == -1) {
                    throw new InvalidRequestStructureException("request line and index's tail not found");
                }
                final String requestLine = new String(buffer, 0, requestLineAndIndex, StandardCharsets.UTF_8);
                log.debug("request line : {}", requestLine);
                final int headersStartIndex = requestLineAndIndex + CRLF.length;
                final int headersEndIndex = Bytes.indexOf(buffer, CRLFCRLF, headersStartIndex);
                if (headersEndIndex == -1) {
                    throw new InvalidRequestStructureException("header's tail not found");
                }

                final String[] requestLineParts = requestLine.split("\\s+", 3);
                if (requestLineParts.length != 3) {
                    throw new InvalidRequestLineStructureException(requestLine);
                }

                request.setMethod(requestLineParts[0]);
                request.setHttpVersion(requestLineParts[2]);

                final String[] pathAndQueryParts = requestLineParts[1].split("\\?", 2);
                final String pathDecoded = URLDecoder.decode(pathAndQueryParts[0], StandardCharsets.UTF_8.name());
                request.setPath(pathDecoded);
                if (pathAndQueryParts.length == 2) {
                    request.setQuery(pathAndQueryParts[1]);
                }

                int lastProcessedIndex = headersStartIndex;
                int contentLength = 0;
                while (lastProcessedIndex < headersEndIndex - CRLF.length) {
                    final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
                    final String currentHeaderLine = new String(buffer, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
                    lastProcessedIndex = currentHeaderEndIndex + CRLF.length;

                    final String[] headersParts = currentHeaderLine.split(":\\s+", 2);
                    if (headersParts.length != 2) {
                        throw new InvalidHeaderLineStructureException(currentHeaderLine);
                    }
                    request.getHeaders().put(headersParts[0], headersParts[1]);

                    if (!headersParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                        continue;
                    }
                    contentLength = Integer.parseInt(headersParts[1]);
                    log.debug("content-length: {}", contentLength);
                }

                if (contentLength > MAX_CONTENT_LENGTH) {
                    throw new RequestBodyTooLargeExeption();
                }

                final int bodyStartIndex = headersEndIndex + CRLFCRLF.length;
                in.reset();
                final long skipped = in.skip(bodyStartIndex);

                final byte[] body = new byte[contentLength];
                int bodyRead = 0;
                while (bodyRead != contentLength) {
                    int currentRead = in.read(body, bodyRead, contentLength - bodyRead);
                    if (currentRead == -1) {
                        break;
                    }
                    bodyRead += currentRead;
                }
                request.setBody(body);

                synchronized (middlewares) {
                    for (Middleware middleware : middlewares) {
                        middleware.handle(socket, request);
                    }
                }

                synchronized (parsers) {
                    for (Parser parser : parsers) {
                        parser.parse(request);
                    }
                }

                final Handler handler;
                Map<HttpMethods, Handler> methodsToHandlers = null;
                synchronized (routes) {
                    for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                        final Matcher matcher = entry.getKey().matcher(request.getPath());
                        if (!matcher.matches()) {
                            continue;
                        }
                        request.setPathMatcher(matcher);
                        methodsToHandlers = entry.getValue();
                    }

                    if (methodsToHandlers == null) {
                        throw new MethodNotAllowedException(request.getMethod());
                    }
                    handler = methodsToHandlers.get(HttpMethods.valueOf(request.getMethod()));
                }
                if (handler == null) {
                    throw new ResourceNotFoundException(request.getPath());
                }

                handler.handle(request, out);
            } catch (MethodNotAllowedException e) {
                log.error("request method not allowed", e);
                methodNotAllowedHandler.handle(request, out);
            } catch (ResourceNotFoundException e) {
                log.error("can't found request ", e);
                notFoundHandler.handle(request, out);
            } catch (Exception e) {
                log.error("can't handle request ", e);
                internalServerErrorHandler.handle(request, out);
            }
        } catch (Exception e) {
            log.error("can't handle request ", e);
        } finally {
            SecurityContext.clear();
        }
    }

    public void stop() throws IOException {
        workers.shutdown();
        try {
            if (!workers.awaitTermination(10, TimeUnit.SECONDS)) {
                workers.shutdownNow();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            workers.shutdownNow();
        } finally {
            serverSocket.close();
        }
    }

    public static class ServerBuilder {
        private ArrayList<Middleware> middlewares;
        private ArrayList<Parser> parsers;
        private ArrayList<Pattern> routesKey;
        private ArrayList<Map<HttpMethods, Handler>> routesValue;
        private Handler notFoundHandler;
        private boolean notFoundHandlerBoolean;
        private Handler internalServerErrorHandler;
        private boolean internalServerErrorHandlerBoolean;
        private Handler methodNotAllowedHandler;
        private boolean methodNotAllowedHandlerBoolean;

        ServerBuilder() {
        }

        public ServerBuilder middleware(Middleware middleware) {
            if (this.middlewares == null) {
                this.middlewares = new ArrayList<>();
            }
            this.middlewares.add(middleware);
            return this;
        }

        public ServerBuilder middlewares(Collection<? extends Middleware> middlewares) {
            if (this.middlewares == null) {
                this.middlewares = new ArrayList<>();
            }
            this.middlewares.addAll(middlewares);
            return this;
        }

        public ServerBuilder clearMiddlewares() {
            if (this.middlewares != null) {
                this.middlewares.clear();
            }
            return this;
        }

        public ServerBuilder parser(Parser parser) {
            if (this.parsers == null) {
                this.parsers = new ArrayList<>();
            }
            this.parsers.add(parser);
            return this;
        }

        public ServerBuilder parsers(Collection<? extends Parser> parsers) {
            if (this.parsers == null) {
                this.parsers = new ArrayList<>();
            }
            this.parsers.addAll(parsers);
            return this;
        }

        public ServerBuilder clearParsers() {
            if (this.parsers != null) {
                this.parsers.clear();
            }
            return this;
        }

        public ServerBuilder route(Pattern routeKey, Map<HttpMethods, Handler> routeValue) {
            if (this.routesKey == null) {
                this.routesKey = new ArrayList<>();
                this.routesValue = new ArrayList<>();
            }
            this.routesKey.add(routeKey);
            this.routesValue.add(routeValue);
            return this;
        }

        public ServerBuilder routes(Map<? extends Pattern, ? extends Map<HttpMethods, Handler>> routes) {
            if (this.routesKey == null) {
                this.routesKey = new ArrayList<>();
                this.routesValue = new ArrayList<>();
            }
            for (final Map.Entry<? extends Pattern, ? extends Map<HttpMethods, Handler>> lombokEntry : routes.entrySet()) {
                this.routesKey.add(lombokEntry.getKey());
                this.routesValue.add(lombokEntry.getValue());
            }
            return this;
        }

        public ServerBuilder clearRoutes() {
            if (this.routesKey != null) {
                this.routesKey.clear();
                this.routesValue.clear();
            }
            return this;
        }

        public ServerBuilder notFoundHandler(Handler notFoundHandler) {
            this.notFoundHandler = notFoundHandler;
            this.notFoundHandlerBoolean = true;
            return this;
        }

        public ServerBuilder internalServerErrorHandler(Handler internalServerErrorHandler) {
            this.internalServerErrorHandler = internalServerErrorHandler;
            this.internalServerErrorHandlerBoolean = true;
            return this;
        }

        public ServerBuilder methodNotAllowedHandler(Handler methodNotAllowedHandler) {
            this.methodNotAllowedHandler = methodNotAllowedHandler;
            this.methodNotAllowedHandlerBoolean = true;
            return this;
        }

        public Server build() {
            List<Middleware> middlewares;
            switch (this.middlewares == null ? 0 : this.middlewares.size()) {
                case 0:
                    middlewares = java.util.Collections.emptyList();
                    break;
                case 1:
                    middlewares = java.util.Collections.singletonList(this.middlewares.get(0));
                    break;
                default:
                    middlewares = java.util.Collections.unmodifiableList(new ArrayList<>(this.middlewares));
            }
            List<Parser> parsers;
            switch (this.parsers == null ? 0 : this.parsers.size()) {
                case 0:
                    parsers = java.util.Collections.emptyList();
                    break;
                case 1:
                    parsers = java.util.Collections.singletonList(this.parsers.get(0));
                    break;
                default:
                    parsers = java.util.Collections.unmodifiableList(new ArrayList<>(this.parsers));
            }

            Map<Pattern, Map<HttpMethods, Handler>> routes;
            switch (this.routesKey == null ? 0 : this.routesKey.size()) {
                case 0:
                    routes = java.util.Collections.emptyMap();
                    break;
                case 1:
                    routes = java.util.Collections.singletonMap(this.routesKey.get(0), this.routesValue.get(0));
                    break;
                default:
                    routes = new java.util.LinkedHashMap<>(this.routesKey.size() < 1073741824 ? 1
                            + this.routesKey.size() + (this.routesKey.size() - 3) / 3 : Integer.MAX_VALUE);
                    for (int i = 0; i < this.routesKey.size(); i++) {
                        routes.put(this.routesKey.get(i), this.routesValue.get(i));
                    }
                    routes = java.util.Collections.unmodifiableMap(routes);

            }
            Handler notFoundHandler = this.notFoundHandler;
            if (!this.notFoundHandlerBoolean) {
                notFoundHandler = Server.notFoundHandler();
            }
            Handler internalServerErrorHandler = this.internalServerErrorHandler;
            if (!this.internalServerErrorHandlerBoolean) {
                internalServerErrorHandler = Server.internalServerErrorHandler();
            }
            Handler methodNotAllowedHandler = this.methodNotAllowedHandler;
            if (!this.methodNotAllowedHandlerBoolean) {
                methodNotAllowedHandler = Server.methodNotAllowedHandler();
            }
            return new Server(middlewares, parsers, routes, notFoundHandler, internalServerErrorHandler, methodNotAllowedHandler);
        }

        public String toString() {
            return "Server.ServerBuilder(middlewares=" + this.middlewares + ", parsers=" + this.parsers + ", routesKey=" + this.routesKey
                    + ", routesValue=" + this.routesValue + ", notFoundHandler=" + this.notFoundHandler +
                    ", internalServerErrorHandler=" + this.internalServerErrorHandler
                    + ", methodNotAllowedHandler=" + this.methodNotAllowedHandler + ")";
        }
    }
}
