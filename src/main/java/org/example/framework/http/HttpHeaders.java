package org.example.framework.http;

public enum HttpHeaders {
    CONTENT_TYPE("Content-Type"),
    CONTENT_LENGTH("Content-Length"),
    CONTENT_DISPOSITION("Content-Disposition"),
    ACCEPT("Accept"),
    AUTHORIZATION("Authorization");

    private final String value;

    HttpHeaders(String content) {
        this.value = content;
    }

    public String value() {
        return value;
    }

}
