package org.example.middleware;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.LoginPrincipal;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.exception.BadAuthenticationException;
import org.example.framework.http.HttpHeaders;
import org.example.framework.http.Request;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@RequiredArgsConstructor
public class BasicAuthNMiddleware implements Middleware {
    private final static String SCHEME = "Basic ";
    private final Authenticator authenticator;

    @Override
    public void handle(final Socket socket, final Request request) {
        if (SecurityContext.getPrincipal() != null) {
            return;
        }
        try {
            final String header = request.getHeaders().get(HttpHeaders.AUTHORIZATION.value());
            if (header == null) {
                return;
            }

            if (!header.startsWith(SCHEME)) {
                return;
            }
            final byte[] decode = Base64.getDecoder().decode(header.substring(SCHEME.length()));
            final String[] parts = new String(decode, StandardCharsets.UTF_8).split(":");
            if (parts.length != 2) {
                log.error("invalid authorization header: {}", header);
                throw new BadAuthenticationException("invalid authorization header format");
            }
            final String login = parts[0];
            final String password = parts[1];

            final LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
            if (authenticator.authenticate(authRequest)) {
                throw new AuthenticationException("can't authenticate");
            }
            SecurityContext.setPrincipal(new LoginPrincipal(login));
        } catch (Exception e) {
            SecurityContext.clear();
        }
    }
}
